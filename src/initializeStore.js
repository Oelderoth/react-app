import {createStore, applyMiddleware} from 'redux';
import allReducers from './reducers/all';
import reduxPromise from 'redux-promise';

export default function initializeStore() {
  return createStore(
    allReducers,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
    applyMiddleware(reduxPromise)
  );
}