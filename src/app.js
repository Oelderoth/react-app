import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import HomePage from './containers/homePage';
import NotFoundPage from './containers/notFoundPage';

import './app.css';

class App extends Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route exact path="/" component={HomePage} />
          <Route component={NotFoundPage}/>
        </Switch>
      </Router>
    );
  }
}

export default App;
