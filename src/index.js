import React from 'react';
import {render} from 'react-snapshot';
import {Provider} from 'react-redux';
import initializeStore from './initializeStore';

import App from './app';

import './index.css';

const store = initializeStore();

render(
    <Provider store={store}>
        <App />
    </Provider>, 
    document.getElementById('root')
);